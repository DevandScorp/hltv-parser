import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { EventsModule } from './events/events.module';

const { PG_HOST, PG_PORT, PG_DATABASE, PG_USERNAME, PG_PASSWORD } = process.env;
@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: PG_HOST,
      port: +PG_PORT,
      username: PG_USERNAME,
      password: PG_PASSWORD,
      database: PG_DATABASE,
      synchronize: true,
      autoLoadModels: true,
    }),
    EventsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
