/**
 * Функция для кэширования запроса получения информации о команде
 */
class AppUtils {
  /** Получение дат текущей недели */
  static getDaysBetweenDates(startOfWeek, endOfWeek) {
    const weekDuration = endOfWeek.diff(startOfWeek, 'd') + 1;
    const dates = [];
    for (let i = 0; i < weekDuration; ++i) {
      dates.push(startOfWeek.clone().add(i, 'd'));
    }
    return dates;
  }
  /** Генерация объекта матча */
  static generateMatch(match) {
    const formattedMatchObject = {
      date: match.date,
      name: match.event?.name || match.title,
      firstTeam: null,
      secondTeam: null,
    };
    /** Получаем информацию по первой команде */
    if (match.team1) {
      formattedMatchObject.firstTeam = {
        id: match.team1.id,
        name: match.team1.name,
      };
    }
    /** Получаем информацию по второй команде */
    if (match.team2) {
      formattedMatchObject.secondTeam = {
        id: match.team2.id,
        name: match.team2.name,
      };
    }
    return formattedMatchObject;
  }
}

export default AppUtils;
