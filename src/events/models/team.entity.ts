import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table
export class Team extends Model {
  @Column({
    type: DataType.BIGINT,
  })
  expirationDate: number;

  @Column({
    unique: true,
  })
  team_id: number;

  @Column
  logo: string;

  @Column
  rank: string;

  @Column
  name: string;
}
