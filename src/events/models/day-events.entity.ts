import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table
export class DayEvents extends Model {
  @Column({
    unique: true,
  })
  day: string;

  @Column({
    type: DataType.BIGINT,
  })
  expirationDate: number;

  @Column({
    type: DataType.JSON,
  })
  events;
}
