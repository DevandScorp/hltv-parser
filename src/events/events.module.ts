import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { DayEvents } from './models/day-events.entity';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';
import { Team } from './models/team.entity';
import { HLTVService } from './hltv.service';

@Module({
  imports: [SequelizeModule.forFeature([DayEvents, Team])],
  controllers: [EventsController],
  providers: [EventsService, HLTVService],
})
export class EventsModule {}
