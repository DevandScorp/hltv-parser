import { IsNotEmpty, Matches } from 'class-validator';

export class LocaleDto {
  @IsNotEmpty()
  @Matches(/^[a-z]{2,4}(-[A-Z][a-z]{3})?(-([A-Z]{2}|[0-9]{3}))?$/)
  locale: string;
}
