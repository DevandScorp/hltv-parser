import {
  Controller,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { LocaleDto } from './dto/locale.dto';
import { EventsService } from './events.service';

@Controller('api')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @Get('events')
  @UsePipes(ValidationPipe)
  async getWeekEvents(@Query() query: LocaleDto) {
    return await this.eventsService.getWeekEvents(query.locale);
  }
}
