import { Injectable } from '@nestjs/common';
import { HLTV } from 'hltv';

@Injectable()
export class HLTVService {
  /** Работа с HLTV выносится в отдельные методы для тестирования */
  getMatches = () => HLTV.getMatches();
  getMatchesStats = (period: { startDate: string; endDate: string }) =>
    HLTV.getMatchesStats(period);
  getMatch = (matchInfo: { id: number }) => HLTV.getMatch(matchInfo);
  getTeam = (teamInfo: { id: number }) => HLTV.getTeam(teamInfo);
}
