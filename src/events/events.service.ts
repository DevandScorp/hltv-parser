import { Injectable, OnApplicationShutdown } from '@nestjs/common';
import { Op } from 'sequelize';
import * as moment from 'moment';
import { HLTV } from 'hltv';
import AppUtils from '../shared/utils';
import { DayEvents } from './models/day-events.entity';
import { UpcomingMatch } from 'hltv/lib/models/UpcomingMatch';
import { Team } from './models/team.entity';
import { InjectModel } from '@nestjs/sequelize';
import { HLTVService } from './hltv.service';

@Injectable()
export class EventsService {
  constructor(
    @InjectModel(DayEvents)
    private readonly dayEventsRepository: typeof DayEvents,
    @InjectModel(Team) private readonly teamRepository: typeof Team,
    private readonly hltvService: HLTVService,
  ) {}
  /**
   * Получени данных о команде из базы или из hltv
   */
  async getTeamInfoCache(team_id: number) {
    const data = await this.teamRepository.findOne({
      where: {
        team_id,
        expirationDate: {
          [Op.gte]: moment().valueOf(),
        },
      },
    });
    if (!data) {
      const teamInfo = await this.hltvService.getTeam({ id: team_id });
      const cleanTeamInfo = {
        expirationDate: moment().add('24', 'h').valueOf(),
        name: teamInfo.name,
        logo: teamInfo.logo,
        rank: teamInfo.rank,
        team_id: teamInfo.id,
      };
      await this.teamRepository.upsert(cleanTeamInfo);
      return cleanTeamInfo;
    }
    return data;
  }
  /**
   * Был применен следующий подход: интервал в неделю с учетом локали разбивался на дни до текущего и дни после текущего вместе с текущим днем.
   * Для дней до не идет проверка на время кэширования, т.к. эти данные уже не будут меняться. Они просто дополняются недостающими днями.
   * Аналогичная проверка происходит и для дней после, однако здесь уже учитывается время кэширования, соотв. все старые записи удаляются.
   * Также для получения информации о команде запросы делаются налету с кэшированием, т.к. эти данные динамические и постоянно обновляются
   * как для прошедших событий, так и для будущих
   */
  async getWeekEvents(locale) {
    const startOfWeek = moment().locale(locale).startOf('week');
    const endOfWeek = moment().locale(locale).endOf('week');
    const weekDays = AppUtils.getDaysBetweenDates(startOfWeek, endOfWeek);
    /** Результирующий массив с данными */
    const result = [];
    const passedWeekDays = weekDays.filter(
      (weekDay) =>
        weekDay < moment(moment().format('YYYY-MM-DD'), 'YYYY-MM-DD'), // для того, чтобы обнулить время текущей даты
    );
    /** Находим прошедшие матчи в базе данных */
    const databasePassedDayEvents = await this.dayEventsRepository.findAll({
      where: {
        day: {
          [Op.in]: passedWeekDays.map((elem) => elem.format('YYYY-MM-DD')),
        },
      },
    });
    /** Записываем результаты из базы в ответ */
    result.push(
      ...databasePassedDayEvents.flatMap(
        (databasePassedDayEvent) => databasePassedDayEvent.events,
      ),
    );
    /** Находим недостающие дни */
    const absentPassedWeekDays = passedWeekDays.filter(
      (passedWeekDay) =>
        !databasePassedDayEvents.find(
          (databasePassedDayEvent) =>
            databasePassedDayEvent.day === passedWeekDay.format('YYYY-MM-DD'),
        ),
    );
    /** Дополняем базу недостающими данными и добавляем их в результирующий массив */
    for (const absentPassedWeekDay of absentPassedWeekDays) {
      const resultDayArray = [];
      const matchesStats = await this.hltvService.getMatchesStats({
        startDate: absentPassedWeekDay.format('YYYY-MM-DD'),
        endDate: absentPassedWeekDay.format('YYYY-MM-DD'),
      });
      /** При получении уже прошедших матчей обнаружилось большое количество дубликатов(разные карты одного матча) => игнорировать одинаковое время*/
      const eventsStatsSet = new Set();
      for (const matchStats of matchesStats) {
        if (eventsStatsSet.has(matchStats.date)) continue;
        else eventsStatsSet.add(matchStats.date);
        resultDayArray.push(AppUtils.generateMatch(matchStats));
      }
      await this.dayEventsRepository.create({
        day: absentPassedWeekDay.format('YYYY-MM-DD'),
        events: resultDayArray,
      });
      result.push(...resultDayArray);
    }
    /** Аналогично проходимся по текущему и дальнейшим дням, но уже с учетом времени кэширования */
    const futureWeekDays = weekDays.filter(
      (weekDay) =>
        weekDay >= moment(moment().format('YYYY-MM-DD'), 'YYYY-MM-DD'),
    );
    /** удаляем те дни, у которых время кэширования истекло */
    await this.dayEventsRepository.destroy({
      where: {
        expirationDate: {
          [Op.lte]: moment().valueOf(),
        },
      },
    });
    /** Находим будущие матчи в базе данных */
    const databaseFutureDayEvents = await this.dayEventsRepository.findAll({
      where: {
        day: {
          [Op.in]: futureWeekDays.map((elem) => elem.format('YYYY-MM-DD')),
        },
      },
    });
    /** Записываем результаты из базы в ответ */
    result.push(
      ...databaseFutureDayEvents.flatMap(
        (databaseFutureDayEvents) => databaseFutureDayEvents.events,
      ),
    );
    /** Находим недостающие дни */
    const absentFutureWeekDays = futureWeekDays.filter(
      (futureWeekDay) =>
        !databaseFutureDayEvents.find(
          (databaseFutureDayEvent) =>
            databaseFutureDayEvent.day === futureWeekDay.format('YYYY-MM-DD'),
        ),
    );
    /** При их наличии получаем будущие матчи и для каждой даты без данных дополняем*/
    const futureMatches = absentFutureWeekDays.length
      ? await this.hltvService.getMatches()
      : [];
    for (const absentFutureWeekDay of absentFutureWeekDays) {
      const matches = futureMatches.filter(
        (futureMatch) =>
          moment((<UpcomingMatch>futureMatch).date).format('YYYY-MM-DD') ===
          absentFutureWeekDay.format('YYYY-MM-DD'),
      );
      const resultDayArray = [];
      for (let match of matches) {
        match = <UpcomingMatch>match;
        /** Если это матч в режиме live, то тогда отдельно получаем дату */
        if (!match.date) {
          const liveMatch = await this.hltvService.getMatch({ id: match.id });
          match = { ...match, date: liveMatch.date };
        }
        resultDayArray.push(AppUtils.generateMatch(match));
      }
      /** Дополняем базу недостающими данными и добавляем их в результирующий массив */
      await this.dayEventsRepository.create({
        day: absentFutureWeekDay.format('YYYY-MM-DD'),
        expirationDate: moment().add('3', 'h').valueOf(),
        events: resultDayArray,
      });
      result.push(...resultDayArray);
    }
    /** Получаем данные о команде и добавляем к итоговым данным */
    const teamsMap = new Map();
    for (const event of result) {
      const firstTeamId = event.firstTeam?.id;
      const secondTeamId = event.secondTeam?.id;
      if (firstTeamId) {
        if (teamsMap.has(firstTeamId)) {
          event.firstTeam = teamsMap.get(firstTeamId);
        } else {
          const team = await this.getTeamInfoCache(firstTeamId);
          event.firstTeam = team;
          teamsMap.set(firstTeamId, team);
        }
      }
      if (secondTeamId) {
        if (teamsMap.has(secondTeamId)) {
          event.secondTeam = teamsMap.get(secondTeamId);
        } else {
          const team = await this.getTeamInfoCache(secondTeamId);
          event.secondTeam = team;
          teamsMap.set(secondTeamId, team);
        }
      }
    }
    return result;
  }
}
