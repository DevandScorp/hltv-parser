import { Test } from '@nestjs/testing';
import { getModelToken } from '@nestjs/sequelize';
import { DayEvents } from './models/day-events.entity';
import { Team } from './models/team.entity';
import { EventsService } from './events.service';
import { HLTVService } from './hltv.service';
import * as moment from 'moment';
import AppUtils from '../shared/utils';

describe('EventsService', () => {
  let eventsService: EventsService;
  let hltvService: HLTVService;
  const testMatch = {
    date: moment().valueOf(),
    name: 'NumberOne Season 1',
    firstTeam: {
      id: 7848,
      name: 'MAESTRO',
    },
    secondTeam: {
      id: 4579,
      name: 'LowLandLions',
    },
  };
  const testTeam = {
    expirationDate: moment().add('24', 'h').valueOf(),
    name: 'Immortals',
    logo: 'https://static.hltv.org/images/team/logo/8675',
    rank: null,
    team_id: 10608,
  };
  const testLocale = 'ru';
  beforeEach(async () => {
    const modRef = await Test.createTestingModule({
      providers: [
        EventsService,
        {
          provide: HLTVService,
          useValue: {
            getMatches: jest.fn(() => []),
            getMatchesStats: jest.fn(() => []),
            getMatch: jest.fn(() => testMatch),
            getTeam: jest.fn(() => testTeam),
          },
        },
        {
          provide: getModelToken(DayEvents),
          useValue: {
            findAll: jest.fn(() => []),
            create: jest.fn(() => null),
            destroy: jest.fn(() => null),
          },
        },
        {
          provide: getModelToken(Team),
          useValue: {
            findOne: jest.fn(() => testMatch),
            upsert: jest.fn(() => testMatch),
          },
        },
      ],
    }).compile();
    eventsService = modRef.get(EventsService);
    hltvService = modRef.get(HLTVService);
  });

  it('getWeekEvents for test locale', async () => {
    const result = await eventsService.getWeekEvents(testLocale);
    expect(result).toBeDefined();
  });
  it('check getMatches method calls', async () => {
    const startOfWeek = moment().locale(testLocale).startOf('week');
    const endOfWeek = moment().locale(testLocale).endOf('week');
    const weekDays = AppUtils.getDaysBetweenDates(startOfWeek, endOfWeek);
    const futureWeekDays = weekDays.filter(
      (weekDay) =>
        weekDay >= moment(moment().format('YYYY-MM-DD'), 'YYYY-MM-DD'),
    );
    await eventsService.getWeekEvents(testLocale);
    const getMacthesSpy = jest.spyOn(hltvService, 'getMatches');
    expect(getMacthesSpy).toBeCalledTimes(+(futureWeekDays.length > 0));
  });
  it('check getMatchesStats method calls', async () => {
    const startOfWeek = moment().locale(testLocale).startOf('week');
    const endOfWeek = moment().locale(testLocale).endOf('week');
    const weekDays = AppUtils.getDaysBetweenDates(startOfWeek, endOfWeek);
    /** Метод должен вызываться для каждого прошедшего дня */
    const passedWeekDays = weekDays.filter(
      (weekDay) =>
        weekDay < moment(moment().format('YYYY-MM-DD'), 'YYYY-MM-DD'),
    );
    await eventsService.getWeekEvents('ru-RU');
    const getMacthesStatsSpy = jest.spyOn(hltvService, 'getMatchesStats');
    expect(getMacthesStatsSpy).toBeCalledTimes(passedWeekDays.length);
  });
});
